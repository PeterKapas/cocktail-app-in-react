import { useState, useContext, useEffect } from 'react'
import { useCallback } from 'react'
import { createContext } from 'react'

const url = 'https://www.thecocktaildb.com/api/json/v1/1/search.php?s='

const AppContext = createContext()

const AppProvider = ({ children }) => {
	const [loading, setLoading] = useState(false)
	const [searchTerm, setSearchTerm] = useState('')
	const [cocktails, setCocktails] = useState([])

	const fetchDrinks = async () => {
		setLoading(true)
		try {
			const response = await fetch(`${url}${searchTerm}`)
			const data = await response.json()
			const { drinks } = data //drinks is an array
			if (drinks) {
				const newCocktails = drinks.map((cocktail) => {
					const { idDrink, strDrink, strDrinkThumb, strAlcoholic, strGlass } =
						cocktail
					return {
						id: idDrink,
						name: strDrink,
						image: strDrinkThumb,
						info: strAlcoholic,
						glass: strGlass,
					}
				})
				setCocktails(newCocktails)
			} else {
				setCocktails([])
			}
			setLoading(false)
		} catch (error) {
			console.log(error)
			setLoading(false)
		}
	}

	useEffect(() => {
		fetchDrinks()
	}, [searchTerm])

	return (
		<AppContext.Provider
			value={{
				loading,
				cocktails,
				setSearchTerm,
			}}
		>
			{children}
		</AppContext.Provider>
	)
}

const useGlobalContext = () => {
	return useContext(AppContext)
}

export { AppProvider, useGlobalContext }
