import { useGlobalContext } from '../context'
import { useEffect, useRef } from 'react'

const SearchForm = () => {
	const { setSearchTerm, cocktails } = useGlobalContext()
	const searchValue = useRef('')

	useEffect(() => {
		searchValue.current.focus()
	}, [])

	const searchCocktail = () => {
		setSearchTerm(searchValue.current.value)
	}

	const handleSubmit = (e) => {
		e.preventDefault()
	}

	const resetSearch = () => {
		setSearchTerm((searchValue.current.value = ''))
	}
	return (
		<section className='section search'>
			<form action='' className='search-form' onSubmit={handleSubmit}>
				<div className='form-control'>
					<label htmlFor='name'>Search your favourite cocktail</label>
					<input
						type='text'
						id='name'
						ref={searchValue}
						onChange={searchCocktail}
					/>
				</div>

				{cocktails.length < 1 ? (
					<div className='btn-container'>
						<button onClick={resetSearch} className='btn btn-primary'>
							Clear search
						</button>
					</div>
				) : null}
			</form>
		</section>
	)
}

export default SearchForm
