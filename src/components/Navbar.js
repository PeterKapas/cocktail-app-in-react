import { Link } from 'react-router-dom'
import logo1 from '../logo1.png'
import { useGlobalContext } from '../context'

const Navbar = () => {
	return (
		<nav className='navbar'>
			<div className='nav-center'>
				<Link to={'/'}>
					<img src={logo1} alt='Peter Coctail House' className='logo' />
				</Link>
			</div>
			<ul className='nav-links'>
				<li>
					<Link to={'/'}>Home</Link>
				</li>
				<li>
					<Link to={'/cocktailrandom'}>Random Cocktail</Link>
				</li>
				<li>
					<Link to={'/about'}>About</Link>
				</li>
				<li>
					<Link to={'/contact'}>Contact</Link>
				</li>
			</ul>
		</nav>
	)
}

export default Navbar
