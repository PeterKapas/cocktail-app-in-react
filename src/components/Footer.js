import styled from 'styled-components'
import { FaGitlab } from 'react-icons/fa'
import { FaLinkedin } from 'react-icons/fa'

const Footer = () => {
	return (
		<FooterStyle className='footer'>
			<Author>
				<Text size='0.6rem'>Created by: </Text>
				<Text size='1.2rem'>Péter Kapás</Text>
			</Author>
			<SocialIcons>
				<Column href='https://gitlab.com/PeterKapas' target='_blank'>
					<FaGitlab size='30' />
					<Text size='0.8rem'>GitLab</Text>
				</Column>
				<Column
					href='https://www.linkedin.com/in/p%C3%A9ter-kap%C3%A1s-32689912b/'
					target='_blank'
				>
					<LinkedInLogo size='30' />
					<Text size='0.8rem'>Linkedin</Text>
				</Column>
			</SocialIcons>
		</FooterStyle>
	)
}
export default Footer

const FooterStyle = styled.footer`
	position: absolute;
	bottom: 0;
	right: 0;
	left: 0;
	display: flex;
	justify-content: space-between;
	align-items: center;
	background-color: var(--offWhite);
	width: var(--smallWidth);
	max-width: var(--maxWidth);
	margin: 0 auto;
	box-shadow: var(--lightShadow);
`
const Author = styled.div`
	width: 40%;
	margin-left: 5%;
	display: flex;
	flex-direction: column;
`

const Text = styled.p`
	margin-top: 0;
	margin-bottom: 0;
	font-size: ${(props) => props.size};
	font-weight: bold;
	color: var(--mainBlack);
	text-align: left;
`
const SocialIcons = styled.div`
	width: 40%;
	margin-right: 5%;
	display: flex;
	justify-content: space-evenly;
	bottom: 20%;
`

const GitLabIcon = styled(FaGitlab)``

const LinkedInLogo = styled(FaLinkedin)``

const Column = styled.a`
	display: flex;
	flex-direction: column;
	justify-content: center;
	color: #000;
	text-decoration: none;
	&:hover {
		color: var(--mainRed);
	}
	:active {
		transform: scale(0.7);
		color: var(--mainBlack);
	}
`
