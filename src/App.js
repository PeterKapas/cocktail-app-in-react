import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About'
import SingleCocktail from './pages/SingleCocktail'
import Error from './pages/Error'
import Navbar from './components/Navbar'
import Contact from './pages/Contact'
import Footer from './components/Footer'
import CocktailRandom from './pages/CocktailRandom'

function App() {
	return (
		<Router>
			<Navbar />
			<Routes>
				<Route exact path='/' element={<Home />} />
				<Route path='/about' element={<About />} />
				<Route path='/cocktail/:id' element={<SingleCocktail />} />
				<Route path='/cocktailrandom' element={<CocktailRandom />} />
				<Route path='/contact' element={<Contact />} />
				<Route path='*' element={<Error />} />
			</Routes>
			<Footer />
		</Router>
	)
}

export default App
