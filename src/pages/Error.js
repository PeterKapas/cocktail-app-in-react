import { Link } from 'react-router-dom'

const Error = () => {
	return (
		<section className='error-page section'>
			<div className='error-container'>
				<h1>Oops! Looks like it is quite empty here...</h1>
				<Link to='/' className='btn btn-primary'>
					Take me Home
				</Link>
			</div>
		</section>
	)
}

export default Error
