import { FaGitlab } from 'react-icons/fa'
import { FaLinkedin } from 'react-icons/fa'

const Contact = () => {
	return (
		<section className='section contact-section'>
			<h1 className='section-title'> Contact</h1>
			<p>
				Please do not hesitate to share your thoughts on this project. We highly
				appreciate your interest. Drink responsibly.
			</p>
			<p>You can reach us at any time via the following links:</p>
			<div className='logo-container'>
				<a
					href='https://www.linkedin.com/in/p%C3%A9ter-kap%C3%A1s-32689912b/'
					target='_blank'
				>
					<FaGitlab size='60' color='red' />
				</a>
				<a href='https://gitlab.com/PeterKapas' target='_blank'>
					<FaLinkedin size='60' />
				</a>
			</div>
		</section>
	)
}
export default Contact
