import Loading from '../components/Loading'
import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'

const url = 'https://www.thecocktaildb.com/api/json/v1/1/random.php'

const CocktailDay = () => {
	const [loading, setLoading] = useState(false)
	const [randomCocktail, setRandomCocktail] = useState(null)

	const fetchRandomCocktail = async () => {
		setLoading(true)
		try {
			const response = await fetch(url)
			const data = await response.json()
			if (data.drinks) {
				console.log(data.drinks)
				const {
					strDrink: name,
					strDrinkThumb: image,
					strAlcoholic: info,
					strCategory: category,
					strGlass: glass,
					strInstructions: instructions,
					strIngredient1,
					strIngredient2,
					strIngredient3,
					strIngredient4,
					strIngredient5,
					strIngredient6,
					strIngredient7,
				} = data.drinks[0]
				const ingredients = [
					strIngredient1,
					strIngredient2,
					strIngredient3,
					strIngredient4,
					strIngredient5,
					strIngredient6,
					strIngredient7,
				]
				const newRandomCocktail = {
					name,
					image,
					info,
					category,
					glass,
					instructions,
					ingredients,
				}
				setRandomCocktail(newRandomCocktail)
			} else {
				setRandomCocktail(null)
			}
			setLoading(false)
		} catch (error) {
			console.log(error)
			setLoading(false)
		}
	}

	useEffect(() => {
		fetchRandomCocktail()
	}, [])

	if (loading) return <Loading />
	if (!randomCocktail)
		return <h2 className='section-title'>No Cocktail to display</h2>

	const { name, image, category, info, glass, instructions, ingredients } =
		randomCocktail
	return (
		<section className='section cocktail-section'>
			<Link to='/' className='btn btn-primary'>
				Back Home
			</Link>
			<h2 className='section-title'>{name}</h2>
			<div className='drink'>
				<img src={image} alt={name} />
				<div className='drink-info'>
					<p>
						<span className='drink-data'> name:</span>
						{name}
					</p>
					<p>
						<span className='drink-data'> category:</span>
						{category}
					</p>
					<p>
						<span className='drink-data'>info :</span>
						{info}
					</p>
					<p>
						<span className='drink-data'> glass:</span>
						{glass}
					</p>
					<p>
						<span className='drink-data'> instructions:</span>
						{instructions}
					</p>
					<p>
						<span className='drink-data '> ingredients:</span>
						{ingredients.map((ingredient, index) => {
							return ingredient ? <span key={index}>{ingredient}</span> : null
						})}
					</p>
				</div>
			</div>
		</section>
	)
}
export default CocktailDay
