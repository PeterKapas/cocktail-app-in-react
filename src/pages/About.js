const About = () => {
	return (
		<section className='section about-section'>
			<h1 className='section-title'> About Us</h1>
			<p>
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet beatae
				blanditiis magnam esse cumque assumenda doloribus, sequi iusto facilis
				vero consequatur, porro atque amet debitis id nulla ipsum quaerat odio.
				Repellendus sed soluta voluptatibus quis, fuga voluptatum debitis quod
				eveniet?
			</p>
		</section>
	)
}

export default About
